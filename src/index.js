const http = require('http');
const { Server } = require('socket.io');

const cors = require('cors');


const server = http.createServer();
const  io =new Server(server,{
  cors: {
    origin: '*', 
  }
});


io.on('connection',(socket)=>{
  console.log('Un cliente se ha conectado');
  console.log('el nuevo cliente es: '+ socket.id);

  io.emit('welcome','bienvenido al servidor socket de Carlos y duvan 😎😎😎  ')

  io.emit('eventosDisponibles',
  'welcome,hello,saludo,generateNumber,random-number,error-message,getColorName,color-name');

  socket.on('hello',(msg)=>{
    console.log(msg);
    io.emit('saludo',`el servidordice hola\n tu mensage es:${msg} ` )
  })

  let randomNumberInterval;

socket.on('generateNumber', (state) => {
  if (state === 'start' && !randomNumberInterval) {
    console.log('Comenzando la generación de números aleatorios');
    randomNumberInterval = setInterval(() => {
      const randomNumber = Math.floor(Math.random() * (10 + 1));
      io.emit('random-number', randomNumber);
    }, 5000);
  } else if (state === 'stop' && randomNumberInterval) {
    console.log('Deteniendo la generación de números aleatorios');
    clearInterval(randomNumberInterval);
    randomNumberInterval = undefined;
  } else {
    console.log('Error: Argumento no válido. Debe ser "start" o "stop".');
    socket.emit('error-message', 'Argumento no válido. Debe ser "start" o "stop".');
  }
});

socket.on('getColorName', (number) => {
  const colorNames = ["Rojo", "Naranja", "Amarillo", "Verde", 
  "Azul", "Índigo", "Violeta", "Negro", "Blanco", "Gris", 'Caramelo'];
  const parsedNumber = parseInt(number);

  if (isNaN(parsedNumber)) {
    console.log('Error: El argumento no es un número válido.');
    socket.emit('error-message', 'El argumento no es un número válido.');
    return;
  }

  if (parsedNumber < 0 || parsedNumber > 10) {
    console.log('Error: El número está fuera del rango válido (0-10).');
    socket.emit('error-message', 'El número está fuera del rango válido (0-10).');
    return;
  }

  const colorName = colorNames[parsedNumber];
  console.log(`El nombre del color para el número ${parsedNumber} es: ${colorName}`);
  socket.emit('color-name', colorName);
});

  socket.on('disconnect', () => {
    console.log('El cliente se ha desconectado');
    clearInterval(randomNumberInterval);
  });
})

const PORT = 3000;
server.listen(PORT, () => {
  console.log(`Servidor Socket.IO escuchando en el puerto ${PORT}`);
});